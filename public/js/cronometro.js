var cronometro = (function () {
    var idCron
    var idCronDec
    var countDown
    var restartCountDown
    var time
    var restartTime;
    var onFinish
    var callBack


    function getTime() {
        return time
    }

    function dec() {
        finalize()
        time--
    }

    function finalize() {
        if (time <= 0) {
            stop()
            onFinish()
            countDown = restartTime
        }
    }

    function initialize(options) {
        time = options.initialTime || 10
        restartTime = time
        countDown = options.countDown || 1000
        restartCountDown = countDown
        onFinish = options.onFinish
        callBack = options.callBack
    }

    function stop() {
        clearInterval(idCron)
        clearInterval(idCronDec)
    }

    function start() {
        console.log('start')
        idCronDec = setInterval(dec, countDown)
        idCron = setInterval(callBack, countDown, getTime)
    }

    function restart() {
        time = restartTime
        countDown = restartCountDown
        stop()
        start()
    }

    return {
        initialize: initialize,
        start: start,
        restart: restart
    }
})();