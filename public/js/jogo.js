function reiniciaJogo() {

    campo.attr("disabled", false)
    campo.val("")

    $("#contador-palavras").text("0")
    $("#contador-caracteres").text("0")
    $("#tempo-digitacao").text(initialTime)

    cronometro.restart()    

    campo.removeClass("campo-desativado")
    campo.removeClass("borda-vermelha")
    campo.removeClass("borda-verde")
}

function finalizaJogo() {
    campo.attr("disabled", true)
    campo.toggleClass("campo-desativado")
    inserePlacar()
}