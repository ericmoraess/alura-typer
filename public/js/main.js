var campo = $(".campo-digitacao");

var initialTime = 10


$(function () {
    atualizaTamanhoFrase();
    inicializaContadores();
    inicializaMarcadores();
    atualizaPlacar();

    // binds
    $("#botao-reiniciar").click(reiniciaJogo);
    $("#botao-placar").click(mostraPlacar);
    $("#botao-frase").click(fraseAleatoria);
    $("#botao-sync").click(sincronizaPlacar);
    
    cronometro.initialize({
        countDown: 1000,
        initialTime: initialTime,
        callBack: atualizarContador,
        onFinish: finalizaJogo
    })
    campo.one("focus", cronometro.start)


    $("#usuarios").selectize({
        create: true,
        sortField: 'text'
    });

    $(".tooltip").tooltipster({
        trigger: "custom"
    });

});

function atualizaTamanhoFrase() {
    var frase = $(".frase").text();
    var numPalavras = frase.split(" ").length;
    var tamanhoFrase = $("#tamanho-frase");
    tamanhoFrase.text(numPalavras);
}

function inicializaContadores() {
    campo.on("input", function () {
        var conteudo = campo.val();

        var qtdPalavras = conteudo.split(" ").length;
        $("#contador-palavras").text(qtdPalavras);

        var qtdCaracteres = conteudo.length;
        $("#contador-caracteres").text(qtdCaracteres);
    });
}

function inicializaMarcadores() {
    campo.on("input", function () {
        var frase = $(".frase").text();
        var digitado = campo.val();
        var comparavel = frase.substr(0, digitado.length);

        if (digitado == comparavel) {
            campo.addClass("borda-verde");
            campo.removeClass("borda-vermelha");
        } else {
            campo.addClass("borda-vermelha");
            campo.removeClass("borda-verde");
        }
    });
}